package com.example.kerastest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.AssetFileDescriptor;
import android.os.Bundle;
import android.util.Log;

import org.tensorflow.lite.Interpreter;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {

            // On va charger le fichier ( pas nécessaire de comprendre )

            AssetFileDescriptor fileDescriptor = getAssets().openFd("model.tflite");

            FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());

            FileChannel fileChannel = inputStream.getChannel();

            long startOffset = fileDescriptor.getStartOffset();
            long declaredLength = fileDescriptor.getDeclaredLength();

            ByteBuffer tfLiteFile = fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset,declaredLength);

            Interpreter interpreter = new Interpreter(tfLiteFile);



            // Tableau d'entrée pour tester le modèle XOR
            float[][] inputArray = {{0,0}, {1,0}, {0,1}, {1,1}};

            // Tableau vierge pour les sorties
            float[][] outputArray = {{0},{0},{0},{0}};

            // On lance le modèle sur les données d'entrée et stocke le résultat dans outputArray
            interpreter.run(inputArray, outputArray);

            // On effectue une prédiction et on affiche les résultats
            // Pour voir le résultat il faut aller dans "LogCat" et chercher "AJ"
            for (float[] out : outputArray){
                Log.d("Activité test","AJ : " + out[0]);
            }

            // On constate que notre modèle a réalisé une prédiction correcte

        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}