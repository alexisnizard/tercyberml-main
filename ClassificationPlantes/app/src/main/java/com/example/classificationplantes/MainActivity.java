package com.example.classificationplantes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.AssetFileDescriptor;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.tensorflow.lite.Interpreter;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class MainActivity extends AppCompatActivity {

    EditText edit1;
    EditText edit2;
    EditText edit3;
    EditText edit4;
    TextView resultatPrediction;

    TextView memoryUsageTextView;

    Button buttonPrediction;

    private Handler handler;
    private int updateInterval = 1000;

    @Override
    protected void onResume() {
        super.onResume();
        handler.post(updateMemoryUsage);
    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacks(updateMemoryUsage);
    }

    private Runnable updateMemoryUsage = new Runnable() {
        @Override
        public void run() {
            Debug.MemoryInfo memoryInfo = new Debug.MemoryInfo();
            Debug.getMemoryInfo(memoryInfo);
            int totalMemory = (int) Runtime.getRuntime().totalMemory();
            int availableMemory = (int) Runtime.getRuntime().freeMemory();
            int usedMemory = totalMemory - availableMemory;
            int memoryUsagePercent = (int)((float)usedMemory / (float)totalMemory * 100);

            memoryUsageTextView = (TextView) findViewById(R.id.useMemory);
            memoryUsageTextView.setText("Utilisation de la mémoire :" + " " + memoryUsagePercent + "%");
            handler.postDelayed(updateMemoryUsage, 200);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edit1 = (EditText) findViewById(R.id.editText1);
        edit2 = (EditText) findViewById(R.id.editText2);
        edit3 = (EditText) findViewById(R.id.editText3);
        edit4 = (EditText) findViewById(R.id.editText4);
        buttonPrediction = (Button) findViewById(R.id.buttonPredict);
        resultatPrediction = (TextView) findViewById(R.id.predictionText);

        handler = new Handler();
        handler.postDelayed(updateMemoryUsage, 200);

        try {

            // On va charger le fichier ( pas nécessaire de comprendre )

            AssetFileDescriptor fileDescriptor = getAssets().openFd("modelPlantes.tflite");

            FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());

            FileChannel fileChannel = inputStream.getChannel();

            long startOffset = fileDescriptor.getStartOffset();
            long declaredLength = fileDescriptor.getDeclaredLength();

            ByteBuffer tfLiteFile = fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset,declaredLength);

            Interpreter interpreter = new Interpreter(tfLiteFile);

            buttonPrediction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (edit1.getText().toString().isEmpty() || edit2.getText().toString().isEmpty() ||
                            edit3.getText().toString().isEmpty() || edit4.getText().toString().isEmpty()) {
                        Toast.makeText(MainActivity.this, "Veuillez remplir tous les champs", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    String sepalLengthString = edit1.getText().toString();
                    float sepalLength = Float.parseFloat(sepalLengthString);

                    String sepalWidthString = edit2.getText().toString();
                    float sepalWidth = Float.parseFloat(sepalWidthString);

                    String petalLengthString = edit3.getText().toString();
                    float petalLength = Float.parseFloat(petalLengthString);

                    String petalWidthString = edit4.getText().toString();
                    float petalWidth = Float.parseFloat(petalWidthString);

                    float[][] inputArray = {{sepalLength, sepalWidth, petalLength, petalWidth}};
                    float[][] outputArray = new float[1][3];
                    String[] plantNames = {"setosa", "versicolor","virginica"};
                    interpreter.run(inputArray, outputArray);
                    int predicted_class = 0;
                    for (int i = 1; i < outputArray[0].length; i++) {
                        if (outputArray[0][i] > outputArray[0][predicted_class]) {
                            predicted_class = i;
                        }
                    }
                    resultatPrediction.setText("La plante est :" + " " + plantNames[predicted_class]);
                }
            });

// Tableau d'entrée pour tester le modèle

// Trouver la classe prédite avec la probabilité la plus élevée
          /*  int predicted_class = np.argmax(outputArray[0]);

// Afficher la classe prédite
            System.out.println("La nouvelle plante appartient à la classe : " + predicted_class);
*/



            /*float[][] inputArray = {{0,0}, {1,0}, {0,1}, {1,1}};

            // Tableau vierge pour les sorties
            float[][] outputArray = {{0},{0},{0},{0}};

            // On lance le modèle sur les données d'entrée et stocke le résultat dans outputArray
            interpreter.run(inputArray, outputArray);

            // On effectue une prédiction et on affiche les résultats
            // Pour voir le résultat il faut aller dans "LogCat" et chercher "AJ"
            for (float[] out : outputArray){
                Log.d("Activité test","AJ : " + out[0]);
            }*/

            // On constate que notre modèle a réalisé une prédiction correcte

        } catch (IOException e) {
            e.printStackTrace();
        }



    }


}