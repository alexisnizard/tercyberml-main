package com.example.terappliv2;

import static android.content.ContentValues.TAG;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import org.tensorflow.lite.Interpreter;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "PermissionExtractor";
    Button btnResultat;
    ListView listeApplis;
    PackageManager packageManager;
    List<ApplicationInfo> applications;

    int cpt=0;

    String[] permissionsList = {"android.permission.BIND_WALLPAPER","android.permission.FORCE_BACK","android.permission.READ_CALENDAR","android.permission.BODY_SENSORS","android.permission.READ_SOCIAL_STREAM","android.permission.READ_SYNC_STATS","android.permission.INTERNET","android.permission.CHANGE_CONFIGURATION","android.permission.BIND_DREAM_SERVICE","android.permission.HARDWARE_TEST","android.permission.BIND_TV_INPUT","android.permission.BIND_VPN_SERVICE","android.permission.BLUETOOTH_PRIVILEGED","android.permission.WRITE_CALL_LOG","android.permission.CHANGE_WIFI_MULTICAST_STATE","android.permission.BIND_INPUT_METHOD","android.permission.SET_TIME_ZONE","android.permission.WRITE_SYNC_SETTINGS","android.permission.WRITE_GSERVICES","android.permission.SET_ORIENTATION","android.permission.BIND_DEVICE_ADMIN","android.permission.MANAGE_DOCUMENTS","android.permission.FORCE_STOP_PACKAGES","android.permission.WRITE_SECURE_SETTINGS","android.permission.CALL_PRIVILEGED","android.permission.MOUNT_FORMAT_FILESYSTEMS","android.permission.SYSTEM_ALERT_WINDOW","android.permission.ACCESS_LOCATION_EXTRA_COMMANDS","android.permission.BRICK","android.permission.DUMP","android.permission.CHANGE_WIFI_STATE","android.permission.RECORD_AUDIO","android.permission.MODIFY_PHONE_STATE","android.permission.READ_PROFILE","android.permission.ACCOUNT_MANAGER","android.permission.SET_ANIMATION_SCALE","android.permission.SET_PROCESS_LIMIT","android.permission.CAPTURE_SECURE_VIDEO_OUTPUT","android.permission.SET_PREFERRED_APPLICATIONS","android.permission.ACCESS_ALL_DOWNLOADS","android.permission.SET_DEBUG_APP","android.permission.STOP_APP_SWITCHES","android.permission.BLUETOOTH","android.permission.ACCESS_WIFI_STATE","android.permission.SET_WALLPAPER_HINTS","android.permission.BIND_NOTIFICATION_LISTENER_SERVICE","android.permission.MMS_SEND_OUTBOX_MSG","android.permission.CONTROL_LOCATION_UPDATES","android.permission.UPDATE_APP_OPS_STATS","android.permission.REBOOT","android.permission.BROADCAST_WAP_PUSH","android.permission.ACCESS_NETWORK_STATE","android.permission.STATUS_BAR","android.permission.WRITE_USER_DICTIONARY","android.permission.BROADCAST_PACKAGE_REMOVED","android.permission.RECEIVE_SMS","android.permission.WRITE_CONTACTS","android.permission.READ_CONTACTS","android.permission.BIND_APPWIDGET","android.permission.SIGNAL_PERSISTENT_PROCESSES","android.permission.INSTALL_LOCATION_PROVIDER","android.permission.ACCESS_DOWNLOAD_MANAGER_ADVANCED","android.permission.WRITE_SETTINGS","android.permission.MASTER_CLEAR","android.permission.READ_INPUT_STATE","android.permission.MANAGE_APP_TOKENS","android.permission.BIND_REMOTEVIEWS","android.permission.BIND_VOICE_INTERACTION","android.permission.BIND_PRINT_SERVICE","android.permission.MODIFY_AUDIO_SETTINGS","android.permission.USE_SIP","android.permission.WRITE_APN_SETTINGS","android.permission.ACCESS_SURFACE_FLINGER","android.permission.FACTORY_TEST","android.permission.READ_LOGS","android.permission.PROCESS_OUTGOING_CALLS","android.permission.UPDATE_DEVICE_STATS","android.permission.SEND_DOWNLOAD_COMPLETED_INTENTS","android.permission.WRITE_CALENDAR","android.permission.NFC","android.permission.MANAGE_ACCOUNTS","android.permission.SEND_SMS","android.permission.INTERACT_ACROSS_USERS_FULL","android.permission.ACCESS_MOCK_LOCATION","android.permission.BIND_ACCESSIBILITY_SERVICE","android.permission.CAPTURE_AUDIO_OUTPUT","android.permission.WRITE_SMS","android.permission.GET_TASKS","android.permission.DELETE_PACKAGES","android.permission.ACCESS_CHECKIN_PROPERTIES","android.permission.SEND_RESPOND_VIA_MESSAGE","android.permission.MEDIA_CONTENT_CONTROL","android.permission.DOWNLOAD_WITHOUT_NOTIFICATION","android.permission.RECEIVE_BOOT_COMPLETED","android.permission.VIBRATE","android.permission.DIAGNOSTIC","android.permission.WRITE_PROFILE","android.permission.CALL_PHONE","android.permission.FLASHLIGHT","android.permission.READ_PHONE_STATE","android.permission.CHANGE_COMPONENT_ENABLED_STATE","android.permission.CLEAR_APP_USER_DATA","android.permission.BROADCAST_SMS","android.permission.KILL_BACKGROUND_PROCESSES","android.permission.READ_FRAME_BUFFER","android.permission.SUBSCRIBED_FEEDS_WRITE","android.permission.CAMERA","android.permission.RECEIVE_MMS","android.permission.WAKE_LOCK","android.permission.ACCESS_DOWNLOAD_MANAGER","android.permission.DELETE_CACHE_FILES","android.permission.RESTART_PACKAGES","android.permission.GET_ACCOUNTS","android.permission.SUBSCRIBED_FEEDS_READ","android.permission.CHANGE_NETWORK_STATE","android.permission.READ_SYNC_SETTINGS","android.permission.DISABLE_KEYGUARD","android.permission.USE_CREDENTIALS","android.permission.READ_USER_DICTIONARY","android.permission.WRITE_MEDIA_STORAGE","android.permission.ACCESS_COARSE_LOCATION","android.permission.SET_POINTER_SPEED","android.permission.BACKUP","android.permission.EXPAND_STATUS_BAR","android.permission.BLUETOOTH_ADMIN","android.permission.ACCESS_FINE_LOCATION","android.permission.LOCATION_HARDWARE","android.permission.PERSISTENT_ACTIVITY","android.permission.REORDER_TASKS","android.permission.BIND_TEXT_SERVICE","android.permission.DEVICE_POWER","android.permission.SET_WALLPAPER","android.permission.READ_CALL_LOG","android.permission.WRITE_EXTERNAL_STORAGE","android.permission.GET_PACKAGE_SIZE","android.permission.WRITE_SOCIAL_STREAM","android.permission.READ_EXTERNAL_STORAGE","android.permission.INSTALL_PACKAGES","android.permission.AUTHENTICATE_ACCOUNTS","android.permission.INTERNAL_SYSTEM_WINDOW","android.permission.CLEAR_APP_CACHE","android.permission.CAPTURE_VIDEO_OUTPUT","android.permission.GET_TOP_ACTIVITY_INFO","android.permission.INJECT_EVENTS","android.permission.SET_ACTIVITY_WATCHER","android.permission.READ_SMS","android.permission.BATTERY_STATS","android.permission.GLOBAL_SEARCH","android.permission.BIND_NFC_SERVICE","android.permission.PACKAGE_USAGE_STATS","android.permission.SET_ALWAYS_FINISH","android.permission.ACCESS_DRM","android.permission.BROADCAST_STICKY","android.permission.MOUNT_UNMOUNT_FILESYSTEMS"};
    List<String> permissionsModel = Arrays.asList(permissionsList);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Affichage de la liste des applications présentes
        listeApplis = findViewById(R.id.listeApplis);
        packageManager = getPackageManager();
        applications = packageManager.getInstalledApplications(PackageManager.GET_META_DATA);

        List<String> nomsApplis = new ArrayList<>();

        for (ApplicationInfo app : applications) {
            nomsApplis.add(packageManager.getApplicationLabel(app).toString());
        }

        ApplicationListAdapter adapter = new ApplicationListAdapter(this, nomsApplis);
        listeApplis.setAdapter(adapter);

        // Bouton affichage du résultat
        btnResultat = findViewById(R.id.boutonResultat);
        btnResultat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cpt++;
                PackageManager packageManager = getPackageManager();
                List<PackageInfo> packageList = packageManager.getInstalledPackages(PackageManager.GET_PERMISSIONS);
                for (PackageInfo packageInfo : packageList) {

                    ApplicationInfo app = packageInfo.applicationInfo;
                    String apkFilePath = app.sourceDir;

                    AssetManager assets = null;
                    try {
                        assets = AssetManager.class.newInstance();
                        Method addAssetPath = AssetManager.class.getDeclaredMethod("addAssetPath", String.class);
                        addAssetPath.invoke(assets, apkFilePath);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }

                    String[] permissions = extractPermissions(assets);
                    String nomAppli = packageManager.getApplicationLabel(packageInfo.applicationInfo).toString();

                    if (permissions.length != 0) {
                        adapter.updateApplicationMalwareStatus(nomAppli, hasMalware(permissions));
                    } else {
                        adapter.updateApplicationMalwareStatus(nomAppli, -1);
                    }
                }
            }
        });
    }

    private float hasMalware(String[] permissions) {
        float[] tableau = new float[permissionsModel.size()];
        for (String permission : permissions) {
            if (permissionsModel.contains(permission)) {
                int index = permissionsModel.indexOf(permission);
                tableau[index] = 1.0f;
            }
        }
        return testModel(tableau);
    }


    public float testModel(float[] permissionstab){

        try {

            // On va charger le fichier ( pas nécessaire de comprendre )

            AssetFileDescriptor fileDescriptor = getAssets().openFd("model_dataset1_91.tflite");

            FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());

            FileChannel fileChannel = inputStream.getChannel();

            long startOffset = fileDescriptor.getStartOffset();
            long declaredLength = fileDescriptor.getDeclaredLength();

            ByteBuffer tfLiteFile = fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset,declaredLength);

            Interpreter interpreter = new Interpreter(tfLiteFile);

            float[][][] inputArray = new float[1][154][1];
//            float[] data = {0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

            for (int i = 0; i < permissionstab.length; i++) {
                inputArray[0][i][0] = permissionstab[i];
            }

            float[][] outputArray = {{0}};
            interpreter.run(inputArray, outputArray);

            return outputArray[0][0];


        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }

        //return outputArray[0][0];
    }


    class ApplicationListAdapter extends ArrayAdapter<String> {
        private final Context context;
        private final List<String> applications;
        private final Map<String, Float> malwareStatus;

        ApplicationListAdapter(Context context, List<String> applications) {
            super(context, android.R.layout.simple_list_item_1, applications);
            this.context = context;
            this.applications = applications;
            this.malwareStatus = new HashMap<>();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView textView = (TextView) super.getView(position, convertView, parent);
            String nomAppli = applications.get(position);
            Float isMalwareObj = malwareStatus.get(nomAppli);
            int isMalwareInt;

            if (isMalwareObj == null) {
                isMalwareInt = -10;
            } else {
                isMalwareInt = (int) (isMalwareObj.floatValue() * 10);
            }

            switch (isMalwareInt) {
                case -10:
                    if (cpt == 0) {
                        textView.setBackgroundColor(getResources().getColor(android.R.color.white));
                    } else {
                        textView.setBackgroundColor(getResources().getColor(R.color.seuil1));
                    }
                    break;
                case 0:
                    textView.setBackgroundColor(getResources().getColor(R.color.seuil1));
                    break;
                case 1:
                    textView.setBackgroundColor(getResources().getColor(R.color.seuil2));
                    break;
                case 2:
                    textView.setBackgroundColor(getResources().getColor(R.color.seuil3));
                    break;
                case 3:
                    textView.setBackgroundColor(getResources().getColor(R.color.seuil4));
                    break;
                case 4:
                    textView.setBackgroundColor(getResources().getColor(R.color.seuil5));
                    break;
                case 5:
                    textView.setBackgroundColor(getResources().getColor(R.color.seuil6));
                    break;
                case 6:
                    textView.setBackgroundColor(getResources().getColor(R.color.seuil7));
                    break;
                case 7:
                    textView.setBackgroundColor(getResources().getColor(R.color.seuil8));
                    break;
                case 8:
                    textView.setBackgroundColor(getResources().getColor(R.color.seuil9));
                    break;
                default:
                    textView.setBackgroundColor(getResources().getColor(R.color.seuil10));
                    break;
            }
            return textView;
        }



        void updateApplicationMalwareStatus(String appName, float isMalware) {
            malwareStatus.put(appName, isMalware);
            notifyDataSetChanged();
        }
    }

    public String[] extractPermissions(AssetManager assets) {
        XmlResourceParser parser = null;

        float[] tableau = new float[154];

        ArrayList<String> permissions = new ArrayList<>();

        try {
            parser = assets.openXmlResourceParser("AndroidManifest.xml");
            int eventType = parser.next();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    String tagName = parser.getName();
                    if ("uses-permission".equals(tagName)) {
                        String permission = parser.getAttributeValue("http://schemas.android.com/apk/res/android", "name");
                        permissions.add(permission);
                    }
                }
                eventType = parser.next();
            }

            parser.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }

//        for (String elementCourant : permissions) {
//            if (permissionsModel.contains(elementCourant)) {
//                int index = permissionsModel.indexOf(elementCourant);
//                tableau[index] = 1.0f;
//            }
//        }

            return permissions.toArray(new String[0]);
    }
}