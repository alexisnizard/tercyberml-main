# IA pour détecter les failles et vulnérabilités dans les applications mobiles

### Introduction
L’objectif de ce TER était d'étudier et de mettre en œuvre **une approche d'apprentissage automatique pour détecter les failles et vulnérabilités dans les applications mobiles**, en se concentrant sur la plateforme Android dans un premier temps, puis en adaptant cette solution à la plateforme iOS par la suite. L'identification des vulnérabilités devait être réalisée **directement depuis le mobile**, au sein d'une application.

Un travail en amont a d'abord été réalisé afin de mieux comprendre les différentes techniques d'identification des failles de sécurité et des vulnérabilités logicielles déjà existantes utilisant comme approche l'apprentissage automatique.

Une fois ces notions assimilées, un article scientifique proposant une implémentation a été sélectionné, puis nous avons dressé un cahier des charges, mettant en évidence les quatre phases majeures définissant ce projet :

- L’implémentation de la solution proposée par l'article
- L'entraînement et l'amélioration du réseau de neurones
- L’analyse des résultats
- La migration de notre implémentation pour la plateforme IOS

Ce TER fait également l'objet d'une **collaboration avec Pradeo**, une entreprise privée opérant dans le domaine de la cybersécurité établie à Montpellier.

L'application mobile a été développée sous Android Studio en utilisant Java comme langage de programmation, et le réseau de neurones a été créé et entraîné à l'aide de Python, avec la bibliothèque TensorFlow.

## Exemple d'affichage
Ci-dessous, vous trouverez un exemple d'affichage du projet :

<p align="center">
  <img src="exemple2.jpg" alt="Exemple d'exécution du programme" width="400">
  <img src="exemple1.jpg" alt="Exemple d'exécution du programme" width="400">
</p>
