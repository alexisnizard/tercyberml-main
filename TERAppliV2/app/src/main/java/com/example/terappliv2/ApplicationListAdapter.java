//package com.example.terappliv2;
//
//import android.content.Context;
//import android.content.Intent;
//import android.content.pm.ApplicationInfo;
//import android.content.pm.PackageManager;
//import android.graphics.drawable.Drawable;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ArrayAdapter;
//import android.widget.BaseAdapter;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import java.util.List;
//
//public class AppListAdapter extends BaseAdapter {
//
//    private List<ApplicationInfo> appList;
//    private LayoutInflater layoutInflater;
//    private PackageManager packageManager;
//
//    AppListAdapter(Context context, List<ApplicationInfo> appList) {
//        this.appList = appList;
//        this.layoutInflater = LayoutInflater.from(context);
//        this.packageManager = context.getPackageManager();
//    }
//
//    @Override
//    public int getCount() {
//        return appList.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return appList.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        if (convertView == null) {
//            convertView = layoutInflater.inflate(R.layout.app_list_item, null);
//        }
//
//        ImageView appIcon = convertView.findViewById(R.id.app_icon);
//        TextView appName = convertView.findViewById(R.id.app_name);
//
//        ApplicationInfo appInfo = appList.get(position);
//
//        appIcon.setImageDrawable(getAppIcon(appInfo.packageName));
//        appName.setText(getAppName(appInfo.packageName));
//
//        return convertView;
//    }
//
//    private Drawable getAppIcon(String packageName) {
//        try {
//            return packageManager.getApplicationIcon(packageName);
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//    private String getAppName(String packageName) {
//        try {
//            return packageManager.getApplicationLabel(packageManager.getApplicationInfo(packageName, 0)).toString();
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//}
